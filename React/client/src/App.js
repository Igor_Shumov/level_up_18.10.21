import React, { useEffect } from "react";
import Header from "./Components/Header/Header";
import TodoListContainer from "./container/TodoListContainer";
import TodoAddContainer from "./container/TodoAddContainer";
import CategoryAddContainer from "./container/CategoryAddContainer";
import CategoryListContainer from "./container/CategoryListContainer";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { useHistory } from "react-router-dom";
import TodoSort from "./Components/Todo/TodoSort";
import ProgressBarContainer from "./container/ProgressBarContainer";
import CategorySearchContainer from "./container/CategorySearchContainer";
import { useDispatch } from "react-redux";
import { actionSaveLocalStorageCategory } from "./Store/CategoryStore/CategoryStore";
import LocalStorageSave from "./Components/LocalStorage/LocalStorageSave";

function App() {
  const history = useHistory();
  const dispatch = useDispatch();
  useEffect(() => {
    history.push("/");
    const getCategoryLocalStorage = localStorage.getItem("categorys");
    const parseCategory = JSON.parse(getCategoryLocalStorage);
    console.log(parseCategory)
    if (parseCategory && parseCategory.length) {
      dispatch(actionSaveLocalStorageCategory(parseCategory));
    };
  }, []);
  return (
    <>
      <Header />
      <ProgressBarContainer />
      <div className="main">
        <div>
          <CategorySearchContainer />
          <TodoSort />
          <LocalStorageSave />
        </div>
        <div className="category__main">
          <CategoryAddContainer />
          <CategoryListContainer />
        </div>
        <div className="todo__main">
          <TodoAddContainer />
          <TodoListContainer />
        </div>
      </div>
    </>
  );
}

export default App;
