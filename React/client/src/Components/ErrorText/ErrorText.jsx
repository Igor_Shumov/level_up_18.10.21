import React from "react";
import "./ErrorText.css";

const ErrorText = ({text}) => {
  return (<h5 className="errorText">{text}</h5>)
};

export default ErrorText;