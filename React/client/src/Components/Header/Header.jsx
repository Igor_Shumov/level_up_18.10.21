import React from "react";
/* import "./Header.css"; */
import { Card } from "react-bootstrap";

class Header extends React.Component {
  render() {
    return (
      <Card>
        <Card.Header as="h5">To Do List</Card.Header>
        <Card.Body>
          <Card.Title>My TO DO LIST</Card.Title>
          <Card.Text>I don't remember anything. I'll write all in my notepad.</Card.Text>
        </Card.Body>
      </Card>
    );
  }
}

export default Header;
