import React from "react";
import { Button } from "react-bootstrap";
import { useSelector } from "react-redux";

const LocalStorageSave = () => {
  const { category } = useSelector((state) => state.reducerCategory);

  const handlerSaveLocalStorage = () => {
    const jsonStringifyCategory = JSON.stringify(category);
    localStorage.setItem("categorys", jsonStringifyCategory);
  };

  return (
    <Button variant="outline-success" onClick={handlerSaveLocalStorage}>
      Save to Local Storage
    </Button>
  );
};

export default LocalStorageSave;
