import React from "react";
import { Form } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { actionTodoSort } from "../../Store/TodoStore/TodoStore";

const sort = [
  { label: "Все заметки", value: "radio1", filtered: "all" },
  { label: "Выполненные", value: "radio2", filtered: "marked" },
  { label: "Невыполненные", value: "radio3", filtered: "unmarked" },
];

const TodoSort = () => {
  const dispatch = useDispatch();
  const handlerSort = (arg) => {
    dispatch(actionTodoSort(arg));
    
  };

  return (
    <>
      <Form className="module">
        {sort.map((item, ind) => (
          <div key={ind} className="radio">
            <Form.Check
              inline
              label={item.label}
              name="group1"
              type="radio"
              value={item.value}
              onChange={() => handlerSort(item.filtered)}
            />
          </div>
        ))}
      </Form>
    </>
  );
};

export default TodoSort;
