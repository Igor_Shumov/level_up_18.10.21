import React from "react";
import { ListGroup, Button } from "react-bootstrap";
import EditTodoContainer from "../../container/TodoEditContainer";

import "./TodoListStyle.css";

const TodoList = ({ todoId, todo, matchTodoId, handlerDeleteTodo, isShowEdit, handlerToggle, handlerCheckTodo }) => {
  return (
    <>
      <ListGroup className="module">
        {todo.length ? (
          todo.map((item) => {
            return (
              item.categoryId === matchTodoId && (
                <React.Fragment key={item.id}>
                  <ListGroup.Item>
                    <div className="list-text">
                      <div>{item.title}</div>
                    </div>
                    <div>
                      <input type="checkbox" onChange={(e) => handlerCheckTodo(e, item.id)} checked={item.checked} />
                      <Button variant="primary" size="sm" onClick={() => handlerToggle(item.id)}>
                        Edit
                      </Button>
                      <Button
                        disabled={item.id === todoId ? isShowEdit : false}
                        onClick={() => handlerDeleteTodo(item.id)}
                        variant="warning"
                        size="sm"
                      >
                        Del
                      </Button>
                    </div>
                  </ListGroup.Item>
                  {item.id === todoId
                    ? isShowEdit && (
                        <EditTodoContainer
                          handlerToggle={handlerToggle}
                          todoId={todoId}
                          title={item.id === todoId ? item.title : ""}
                        />
                      )
                    : null}
                </React.Fragment>
              )
            );
          })
        ) : (
          <p className="emptyList">Заметки отсутствуют</p>
        )}
      </ListGroup>
    </>
  );
};

export default TodoList;
