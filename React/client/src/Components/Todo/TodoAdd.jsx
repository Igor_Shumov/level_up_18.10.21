import React from "react";
import { InputGroup, FormControl, Button } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";

const TodoAdd = ({ handlerTodoAdd, value, error, handlerChange }) => {
  return (
    <div className="module">
      <InputGroup className="mb-3">
        <FormControl placeholder="Write your note" value={value} onChange={handlerChange}/>
        <Button variant="outline-primary" onClick={handlerTodoAdd}>
          Write
        </Button>
      </InputGroup>
      {error.messages && <ErrorText text={error.messages} />}
    </div>
  );
};

export default TodoAdd;
