import React from "react";
import { InputGroup, FormControl, Button } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";

const EditTodo = ({ value, handlerChange, handlerEditTodo, isShow }) => {
  return (
    <>
      <InputGroup className="mb-3">
        <FormControl onChange={handlerChange} value={value} />
        <Button variant="primary" size="sm" onClick={handlerEditTodo}>
          Edit & go
        </Button>
      </InputGroup>
      {isShow && <ErrorText text="Заметка не должна быть пустой!" />}
    </>
  );
};

export default EditTodo;
