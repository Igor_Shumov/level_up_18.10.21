import React from "react";
import { InputGroup, FormControl, Button } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";

const EditCategory = ({ value, handlerChange, handlerEditCategory, isShow }) => {
  return (
    <>
      <InputGroup className="mb-3">
        <FormControl onChange={handlerChange} value={value} />
        <Button variant="primary" size="sm" onClick={handlerEditCategory}>
          Edit
        </Button>
      </InputGroup>
      {isShow && <ErrorText text="Категория не должна быть пустой!" />}
    </>
  );
};

export default EditCategory;
