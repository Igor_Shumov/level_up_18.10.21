import React from "react";
import { InputGroup, Button, FormControl } from "react-bootstrap";
import ErrorText from "../ErrorText/ErrorText";

const CategoryAdd = ({ handlerCategoryAdd, value, isShow, handlerChange }) => {
  return (
    <div className="module">
      <InputGroup className="mb-3">
        <FormControl placeholder="Create a category right here!" value={value} onChange={handlerChange} />
        <Button variant="outline-success" onClick={handlerCategoryAdd}>
          Add category
        </Button>
      </InputGroup>
      {isShow && <ErrorText text="Категория не должна быть пустой!" />}
    </div>
  );
};

export default CategoryAdd;