import React from "react";
import { ListGroup, Button } from "react-bootstrap";
import EditCategoryContainer from "../../container/CategoryEditContainer";
import { Link } from "react-router-dom";

const CategoryList = ({  categoryId, categoryList, handlerDeleteCategory, isShowEdit, handlerToggle }) => {
  return (
    <ListGroup className="module">
      {categoryList.length ? (
        categoryList.map((item) => {
          return (
            <React.Fragment key={item.id}>
              <ListGroup.Item>
                <Link to={`/category/${item.id}`}>
                  <div className="list-text">{item.title}</div>
                </Link>
                <div>
                  <Button variant="success" size="sm" onClick={() => handlerToggle(item.id)}>
                    Edit
                  </Button>
                  <Button
                    disabled={item.id === categoryId ? isShowEdit : false}
                    onClick={() => handlerDeleteCategory(item.id)}
                    variant="warning"
                    size="sm"
                  >
                    Del
                  </Button>
                </div>
              </ListGroup.Item>
              {item.id === categoryId
                ? isShowEdit && (
                    <EditCategoryContainer
                      handlerToggle={handlerToggle}
                      categoryId={categoryId}
                      title={item.id === categoryId ? item.title : ""}
                    />
                  )
                : null}
            </React.Fragment>
          );
        })
      ) : (
        <p className="emptyList">Категории отсутствуют</p>
      )}
    </ListGroup>
  );
};

export default CategoryList;
