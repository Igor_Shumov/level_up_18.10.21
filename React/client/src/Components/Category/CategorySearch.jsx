import React from "react";
import { InputGroup, FormControl } from "react-bootstrap";

const CategorySearch = ({ value, handleChange }) => {
  return (
    <InputGroup size="sm" className="module">
      <InputGroup.Text id="inputGroup-sizing-sm">Search category</InputGroup.Text>
      <FormControl aria-label="Small" aria-describedby="inputGroup-sizing-sm" value={value} onChange={handleChange} />
    </InputGroup>
  );
};

export default CategorySearch;
