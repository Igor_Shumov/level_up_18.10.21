import React from "react";
import { ProgressBar } from "react-bootstrap";

const ProgBar = ( {completedTodo} ) => {
  return (
    <div className="module">
      <ProgressBar striped now={completedTodo} />
    </div>
  );
};

export default ProgBar;