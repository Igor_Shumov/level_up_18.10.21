import { combineReducers, createStore, applyMiddleware } from "redux";
import reducerTodo from "./TodoStore/TodoStore";
import reducerCategory from "./CategoryStore/CategoryStore";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

export const rootReducers = combineReducers({
  reducerTodo,
  reducerCategory,
});

export const configureStore = (initState) => {
  const store = createStore(rootReducers, initState, composeWithDevTools(applyMiddleware(thunk)));
  return store;
};
