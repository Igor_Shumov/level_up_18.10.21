const initState = {
  category: [],
};

const actionType = {
  ADD_CATEGORY: "ADD_CATEGORY",
  DELETE_CATEGORY: "DELETE_CATEGORY",
  EDIT_CATEGORY: "EDIT_CATEGORY",
  SAVE_LOCAL_STORAGE_CATEGORY: "SAVE_LOCAL_STORAGE_CATEGORY",
};

export const actionCategoryAdd = (payload) => ({
  type: actionType.ADD_CATEGORY,
  payload,
});

export const actionCategoryDelete = (payload) => ({
  type: actionType.DELETE_CATEGORY,
  payload,
});

export const actionCategoryEdit = (payload) => ({
  type: actionType.EDIT_CATEGORY,
  payload,
});

export const actionSaveLocalStorageCategory = (payload) => ({
  type: actionType.SAVE_LOCAL_STORAGE_CATEGORY,
  payload,
});

function reducerCategory(state = initState, action) {
  switch (action.type) {
    case actionType.ADD_CATEGORY:
      return { ...state, category: [...state.category, action.payload] };
    case actionType.DELETE_CATEGORY:
      return { ...state, category: state.category.filter((item) => item.id !== action.payload) };
    case actionType.EDIT_CATEGORY:
      return {
        ...state,
        category: state.category.map((item) => (item.id === action.payload.id ? { ...item, ...action.payload } : item)),
      };
    case actionType.SAVE_LOCAL_STORAGE_CATEGORY:
      console.log(action.payload)
      return { ...state, category: [...state.category, ...action.payload] };
    default:
      return state;
  }
}

export default reducerCategory;
