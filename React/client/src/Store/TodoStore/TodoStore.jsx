const initState = {
  todo: [],
  todoFilter: 'All',
};

const actionType = {
  ADD_TODO: "ADD_TODO",
  DELETE_TODO: "DELETE_TODO",
  EDIT_TODO: "EDIT_TODO",
  CHECK_TODO: "CHECK_TODO",
  SORT_TODO: "SORT_TODO",
};

export const actionTodoAdd = (payload) => ({
  type: actionType.ADD_TODO,
  payload,
});

export const actionTodoDelete = (payload) => ({
  type: actionType.DELETE_TODO,
  payload,
});

export const actionTodoEdit = (payload) => ({
  type: actionType.EDIT_TODO,
  payload,
});

export const actionTodoCheck = (payload) => ({
  type: actionType.CHECK_TODO,
  payload,
});

export const actionTodoSort = (payload) => ({
  type: actionType.SORT_TODO,
  payload,
});

function reducerTodo(state = initState, action) {
  switch (action.type) {
    case actionType.ADD_TODO:
      return { ...state, todo: [...state.todo, action.payload] };
    case actionType.DELETE_TODO:
      return { ...state, todo: state.todo.filter((item) => item.id !== action.payload) };
    case actionType.EDIT_TODO:
      return {
        ...state,
        todo: state.todo.map((item) => (item.id === action.payload.id ? { ...item, ...action.payload } : item)),
      };
    case actionType.CHECK_TODO:
      return {
        ...state,
        todo: state.todo.map((item) => (item.id === action.payload.todoId ? { ...item, ...action.payload } : item)),
      };
    case actionType.SORT_TODO:
      return {
        ...state, todoFilter: action.payload 
      };
    default:
      return state;
  }
}

export default reducerTodo;
