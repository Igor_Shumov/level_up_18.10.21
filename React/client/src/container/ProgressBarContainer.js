import React from "react";
import { useSelector } from "react-redux";
import ProgBar from "../Components/ProgressBar/ProgressBar";

const ProgressBarContainer = () => {
  const { todo } = useSelector((state) => state.reducerTodo);
  const filteredTodo = todo.filter((item) => item.checked);
  const completedTodo = (100 * filteredTodo.length) / todo.length;

  return <ProgBar completedTodo={completedTodo} />;
};

export default ProgressBarContainer;
