import React, { useState } from "react";
import EditTodo from "../Components/Todo/TodoEdit";
import { useDispatch } from "react-redux";
import { actionTodoEdit } from "../Store/TodoStore/TodoStore";

const EditTodoContainer = ({ title, todoId, handlerToggle}) => {
  const [value, setValue] = useState(title);
  const [isShow, SetIsShow] = useState(false);

  const dispatch = useDispatch();

  const handlerChange = (e) => {
    setValue(e.target.value);
  };
  const handlerEditTodo = () => {
    const data = {
      title: value,
      id: todoId,
    };
    handlerToggle(null, true);
    if (!value.trim().length) {
      return SetIsShow(true);
    }
    dispatch(actionTodoEdit(data));
    SetIsShow(false);
  };

  return (
    <EditTodo
      value={value}
      handlerChange={handlerChange}
      handlerEditTodo={handlerEditTodo}
      isShow={isShow}
    />
  );
};

export default EditTodoContainer;
