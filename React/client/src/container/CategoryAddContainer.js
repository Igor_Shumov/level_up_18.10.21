import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { actionCategoryAdd } from "../Store/CategoryStore/CategoryStore";
import CategoryAdd from "../Components/Category/CategoryAdd";

const CategoryAddContainer = () => {
  const [value, SetValue] = useState("");
  const [isShow, SetIsShow] = useState(false);
  const dispatch = useDispatch();

  const handlerChange = (e) => {
    SetValue(e.target.value);
  };

  const handlerCategoryAdd = () => {
    const data = {
      title: value,
      id: Date.now(),
    };
    if (!value.trim().length) {
      return SetIsShow(true);
    }
    dispatch(actionCategoryAdd(data));
    SetValue("");
    SetIsShow(false);
  };

  return (
    <CategoryAdd handlerCategoryAdd={handlerCategoryAdd} value={value} isShow={isShow} handlerChange={handlerChange} />
  );
};

export default CategoryAddContainer;
