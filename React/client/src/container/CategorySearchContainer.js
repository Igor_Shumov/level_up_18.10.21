import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import CategorySearch from '../Components/Category/CategorySearch'

const CategorySearchContainer = () => {
  const [value, setValue] = useState("");

  const url = new URL(window.location.href);
  const history = useHistory();

  const handleChange = (e) => {
    const setValueUrl = url.searchParams.set("search", `${e.target.value}`);
    history.replace(url.search.replace(setValueUrl));
    setValue(e.target.value);
  };

  return <CategorySearch value={value} handleChange={handleChange} />;
};

export default CategorySearchContainer;