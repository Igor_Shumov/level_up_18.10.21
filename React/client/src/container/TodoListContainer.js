import React, { useEffect, useState } from "react";
import TodoList from "../Components/Todo/TodoList";
import { useDispatch, useSelector } from "react-redux";
import { actionTodoCheck, actionTodoDelete, actionTodoSort } from "../Store/TodoStore/TodoStore";
import { useRouteMatch } from "react-router-dom";

const TodoListContainer = () => {
  const { todo, todoFilter } = useSelector((state) => state.reducerTodo);
  const [isShowEdit, setIsShowEdit] = useState(false);
  const [todoId, setTodoId] = useState("");
  const [todoState, setTodoState] = useState(todo);

  const match = useRouteMatch("/category/:id");
  const matchTodoId = match?.params.id;

  const dispatch = useDispatch();

  const handlerCheckTodo = (e, todoId) => {
    const data = {
      todoId,
      checked: e.target.checked,
    };
    dispatch(actionTodoCheck(data));
  };

  const handlerDeleteTodo = (id) => dispatch(actionTodoDelete(id));

  const handlerToggle = (id, isClosed) => {
    setTodoId(id);
    setIsShowEdit(!isShowEdit);
    if (isClosed) {
      setIsShowEdit(false);
    }
  };
  useEffect(() => {
    if (matchTodoId) {
      dispatch(actionTodoSort("all"));
      
    }
  }, [matchTodoId]);

  useEffect(() => {
    setTodoState(todo);
    if (todoFilter === "marked") {
      return setTodoState(todo.filter((item) => item.checked));
    }
    if (todoFilter === "unmarked") {
      return setTodoState(todo.filter((item) => !item.checked));
    }
    return todoFilter;
  }, [todo, todoFilter]);

  return (
    <TodoList
      todo={todoState}
      todoId={todoId}
      matchTodoId={matchTodoId}
      isShowEdit={isShowEdit}
      handlerDeleteTodo={handlerDeleteTodo}
      handlerToggle={handlerToggle}
      handlerCheckTodo={handlerCheckTodo}
    />
  );
};

export default TodoListContainer;
