import React, { useState } from "react";
import EditCategory from "../Components/Category/CategoryEdit";
import { useDispatch } from "react-redux";
import { actionCategoryEdit } from "../Store/CategoryStore/CategoryStore";

const EditCategoryContainer = ({ title, categoryId, handlerToggle}) => {
  const [value, setValue] = useState(title);
  const [isShow, SetIsShow] = useState(false);

  const dispatch = useDispatch();

  const handlerChange = (e) => {
    setValue(e.target.value);
  };
  const handlerEditCategory = () => {
    const data = {
      title: value,
      id: categoryId,
    };
    handlerToggle(null, true);
    if (!value.trim().length) {
      return SetIsShow(true);
    }
    dispatch(actionCategoryEdit(data));
    SetIsShow(false);
  };

  return (
    <EditCategory
      value={value}
      handlerChange={handlerChange}
      handlerEditCategory={handlerEditCategory}
      isShow={isShow}
    />
  );
};

export default EditCategoryContainer;
