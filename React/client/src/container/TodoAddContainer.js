import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { actionTodoAdd } from "../Store/TodoStore/TodoStore";
import TodoAdd from "../Components/Todo/TodoAdd";
import { useRouteMatch } from "react-router-dom";

const TodoAddContainer = () => {
  const [value, SetValue] = useState("");
  const [error, SetError] = useState({});
  const match = useRouteMatch("/category/:id");
  const matchTodoId = match?.params.id;

  const dispatch = useDispatch();

  const handlerChange = (e) => {
    SetValue(e.target.value);
  };

  const handlerTodoAdd = () => {
    const data = {
      title: value,
      id: Date.now(),
      categoryId: matchTodoId,
      checked: false
    };
    if (!value.trim().length) {
      return SetError({ messages: "Заметка не должна быть пустой!" });
    };

    if (!matchTodoId) {
      return SetError({ messages: "Выберите категорию" });
    };
    dispatch(actionTodoAdd(data));
    SetError({})
    SetValue("");
  };

  return (
    <TodoAdd
      handlerTodoAdd={handlerTodoAdd}
      value={value}
      error={error}
      handlerChange={handlerChange}
      matchTodoId={matchTodoId}
    />
  );
};

export default TodoAddContainer;
