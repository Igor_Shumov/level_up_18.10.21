import React, { useState } from "react";
import CategoryList from "../Components/Category/CategoryList";
import { useDispatch, useSelector } from "react-redux";
import { actionCategoryDelete } from "../Store/CategoryStore/CategoryStore";
import { useLocation, useRouteMatch } from "react-router-dom";

const CategoryListContainer = () => {
  const categoryList = useSelector((state) => state.reducerCategory);
  const [isShowEdit, setIsShowEdit] = useState(false);
  const [categoryId, setCategoryId] = useState("");

  const match = useRouteMatch("/category/:id");
  const matchTodoId = match?.params.id;

  const dispatch = useDispatch();
  const location = useLocation();

  const getValuesSearch = new URLSearchParams(location.search).get("search");

  const filteredCategoryList = getValuesSearch
    ? categoryList.category.filter((item) =>
        item.title.toLocaleLowerCase().includes(getValuesSearch.toLocaleLowerCase())
      )
    : categoryList.category;

  const handlerDeleteCategory = (id) => dispatch(actionCategoryDelete(id));

  const handlerToggle = (id, isClosed) => {
    setCategoryId(id);
    setIsShowEdit(!isShowEdit);
    if (isClosed) {
      setIsShowEdit(false);
    }
  };

  return (
    <CategoryList
      categoryId={categoryId}
      isShowEdit={isShowEdit}
      categoryList={filteredCategoryList}
      handlerDeleteCategory={handlerDeleteCategory}
      handlerToggle={handlerToggle}
      matchTodoId={matchTodoId}
    />
  );
};

export default CategoryListContainer;
